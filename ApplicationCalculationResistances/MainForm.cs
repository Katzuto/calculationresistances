﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using ApplicationCalculationResistances.Interpreter;
using ModelCalculationResistances;
using IComponent = ModelCalculationResistances.IComponent;

namespace ApplicationCalculationResistances
{
	public partial class MainForm : Form
	{
		/// <summary>
		/// Основная цепь
		/// </summary>
		private IComponent _allCircuit; 

		public MainForm()
		{
			InitializeComponent();

			CapacitorButton.Click += ActionButton_Click;
			ResistanceButton.Click += ActionButton_Click;
			InductorButton.Click += ActionButton_Click;

			DeleteButton.Click += ActionButton_Click;

			ChangeValueButton.Click += ActionButton_Click;
		}

		private void firstCircuitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			// Первая схема
			IComponent CircuitFirst = new SerialCircuit();

			IComponent ElementResistor = new Resistor(5D);

			IComponent CircuitSecond = new ParallelCircuit();

			IComponent ElementCapacitor = new Capacitor(8D);

			IComponent ElementInductor = new Inductor(12D);

			CircuitSecond.Add(ElementInductor);
			CircuitSecond.Add(ElementCapacitor);

			CircuitFirst.Add(ElementResistor);
			CircuitFirst.Add(CircuitSecond);

			_allCircuit = CircuitFirst;

			TreeNode newNode = Interpreter.Interpreter.InterpreterCircuitInTreeNode(_allCircuit);

			CircuitTree.Nodes.Clear();
			CircuitTree.Nodes.Add(newNode);

			ActionButton_Click(sender, e);
		}

		private void secondCircuitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			// Вторая схема
			IComponent CircuitFirst = new SerialCircuit();

			IComponent CircuitSecond = new ParallelCircuit();

			IComponent ElementResistor = new Resistor(5D);

			IComponent ElementCapacitor = new Capacitor(8D);

			IComponent ElementInductor = new Inductor(12D);

			CircuitSecond.Add(ElementResistor);
			CircuitSecond.Add(ElementCapacitor);

			CircuitFirst.Add(CircuitSecond);
			CircuitFirst.Add(ElementInductor);

			_allCircuit = CircuitFirst;

			TreeNode newNode = Interpreter.Interpreter.InterpreterCircuitInTreeNode(_allCircuit);

			CircuitTree.Nodes.Clear();
			CircuitTree.Nodes.Add(newNode);

			ActionButton_Click(sender, e);
		}

		private void thirdCircuitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			IComponent CircuitFirst = new SerialCircuit();

			IComponent CircuitSecond = new ParallelCircuit();

			IComponent CircuitThird = new ParallelCircuit();

			IComponent ElementCapacitor = new Capacitor(8D);

			IComponent ElementInductor = new Inductor(12D);

			CircuitSecond.Add(ElementCapacitor);
			CircuitSecond.Add(ElementInductor);

			CircuitThird.Add(ElementInductor);
			CircuitThird.Add(ElementCapacitor);

			CircuitFirst.Add(CircuitSecond);
			CircuitFirst.Add(ElementInductor);
			CircuitFirst.Add(CircuitThird);

			_allCircuit = CircuitFirst;

			TreeNode newNode = Interpreter.Interpreter.InterpreterCircuitInTreeNode(_allCircuit);

			CircuitTree.Nodes.Clear();
			CircuitTree.Nodes.Add(newNode);

			ActionButton_Click(sender, e);
		}

		private void fourthCircuitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			IComponent CircuitFirst = new ParallelCircuit();

			IComponent CircuitSecond = new SerialCircuit();

			IComponent CircuitThird = new ParallelCircuit();

			IComponent CircuitFourth = new SerialCircuit();

			IComponent CircuitFifth = new ParallelCircuit();

			IComponent ElementResistor = new Resistor(5D);

			IComponent ElementCapacitor = new Capacitor(8D);

			IComponent ElementInductor = new Inductor(12D);

			CircuitThird.Add(ElementResistor);
			CircuitThird.Add(ElementInductor);

			CircuitSecond.Add(ElementCapacitor);
			CircuitSecond.Add(CircuitThird);

			CircuitFifth.Add(ElementInductor);
			CircuitFifth.Add(ElementResistor);

			CircuitFourth.Add(CircuitFifth);
			CircuitFourth.Add(ElementCapacitor);

			CircuitFirst.Add(CircuitSecond);
			CircuitFirst.Add(CircuitFourth);

			_allCircuit = CircuitFirst;

			TreeNode newNode = Interpreter.Interpreter.InterpreterCircuitInTreeNode(_allCircuit);

			CircuitTree.Nodes.Clear();
			CircuitTree.Nodes.Add(newNode);

			ActionButton_Click(sender, e);
		}

		private void fifthCircuitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			IComponent CircuitFirst = new SerialCircuit();

			IComponent CircuitSecond = new ParallelCircuit();

			IComponent CircuitThird = new SerialCircuit();

			IComponent CircuitFourth = new ParallelCircuit();

			IComponent CircuitFifth = new SerialCircuit();

			IComponent ElementResistor = new Resistor(5D);

			IComponent ElementCapacitor = new Capacitor(8D);

			IComponent ElementInductor = new Inductor(12D);

			CircuitFourth.Add(ElementCapacitor);
			CircuitFourth.Add(ElementResistor);

			CircuitThird.Add(ElementInductor);
			CircuitThird.Add(CircuitFourth);

			CircuitFifth.Add(ElementResistor);
			CircuitFifth.Add(ElementInductor);

			CircuitSecond.Add(ElementInductor);
			CircuitSecond.Add(CircuitThird);
			CircuitSecond.Add(CircuitFifth);

			CircuitFirst.Add(ElementInductor);
			CircuitFirst.Add(CircuitSecond);

			_allCircuit = CircuitFirst;

			TreeNode newNode = Interpreter.Interpreter.InterpreterCircuitInTreeNode(_allCircuit);

			CircuitTree.Nodes.Clear();
			CircuitTree.Nodes.Add(newNode);

			ActionButton_Click(sender, e);
		}


		/// <summary>
		/// Кнопка "ActionButton"
		/// </summary>
		private void ActionButton_Click(object sender, EventArgs e)
		{
			// Очистить панель
			PaintPanel.CreateGraphics().Clear(Color.White);

			try
			{
				// Проверяем пустая ли цепь
				if ((CircuitTree.GetNodeCount(false) == 0))
				{
					throw new ApplicationException("Please, create circuit.");
				}

				// Проверяем есть ли в цепи элементы кроме Параллельно или Последовательного
				// соединения
				if (!(TreeViewChecking.CheckElementTreeView(CircuitTree.Nodes[0])))
				{
					throw new ApplicationException("Please, add elements in circuit.");
				}

				if (FrequencyEdit.Text == "")
				{
					FrequencyEdit.Focus();
					throw new ApplicationException("Please, enter frequency.");
				}

				_allCircuit = IndetificationCircuit(CircuitTree.Nodes[0]);

				IComponentDrawer componentDrawer = Interpreter.Interpreter.InterpreterCircuitInPicture(_allCircuit);

				var image = componentDrawer.DrawComponent();
				
				float x = (PaintPanel.Width / 2) - (image.Width / 2);
				float y = (PaintPanel.Height / 2) - (image.Height / 2);				

				PaintPanel.CreateGraphics().DrawImage(image, x, y);

				ShowImpedance();
			}
			catch (ApplicationException exception)
			{
				MessageBox.Show(exception.Message);
			}		
		}

        
		/// <summary>
		/// Кнопка "SerialButton"
		/// </summary>
		private void SerialCircuitButton_Click(object sender, EventArgs e)
		{
			AddCircuitInTree("Serial");
		}


		/// <summary>
		/// Кнопка "ParallelCircuit"
		/// </summary>
		private void ParallelCircuitButton_Click(object sender, EventArgs e)
		{
			AddCircuitInTree("Parallel");
		}


		/// <summary>
		/// Кнопка "CapacitorButton"
		/// </summary>
		private void CapacitorButton_Click(object sender, EventArgs e)
		{
			AddElementInTree("Capacitor");
		}


		/// <summary>
		/// Кнопка "ResistaceButton"
		/// </summary>
		private void ResistanceButton_Click(object sender, EventArgs e)
		{
			AddElementInTree("Resistor");
		}


		/// <summary>
		/// Кнопка "InductorButton"
		/// </summary>
		private void InductorButton_Click(object sender, EventArgs e)
		{
			AddElementInTree("Inductor");
		}


		/// <summary>
		/// Создание дерева цепи по TreeView
		/// </summary>
		/// <param name="treeNode"> Первый элемент TreeView </param>
		private IComponent IndetificationCircuit(TreeNode treeNode)
		{
			IComponent component;

			switch (treeNode.Text)
			{
				case "Serial":
					component = new SerialCircuit();
					foreach (TreeNode tNode in treeNode.Nodes)
					{
						component.Add(IndetificationCircuit(tNode));
					}
					break;
				case "Parallel":
					component = new ParallelCircuit();
					foreach (TreeNode tNode in treeNode.Nodes)
					{
						component.Add(IndetificationCircuit(tNode));
					}
					break;
				case "Resistor":
					return new Resistor(Convert.ToDouble(treeNode.Tag));
					break;
				case "Capacitor":
					return new Capacitor(Convert.ToDouble(treeNode.Tag));
					break;
				case "Inductor":
					return new Inductor(Convert.ToDouble(treeNode.Tag));
					break;
				default:
					component = null;
					break;
			}

			return component;
		}


		/// <summary>
		/// Ограничиваем ввод поля ValueElementEdit
		/// </summary>
		private void ValueElementEdit_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				ChangeValueButton_Click(sender, null);
			}

			ValueElementChecking.CheckOnlyNumbers(e);
		}


		/// <summary>
		/// Ограничиваем ввод поля FrequencyEdit
		/// </summary>
		private void FrequencyEdit_KeyPress(object sender, KeyPressEventArgs e)
		{
			ValueElementChecking.CheckOnlyNumbers(e);
		}


		/// <summary>
		/// Кнопка "DeleteButton"
		/// </summary>
		private void DeleteButton_Click(object sender, EventArgs e)
		{
			CircuitTree.SelectedNode.Remove();

			try
			{
				if (!(TreeViewChecking.CheckElementTreeView(CircuitTree.Nodes[0])))
				{
					DeleteButton.Click -= ActionButton_Click;
				}
			}
			// TODO: Добавить внутриности!
			catch (ArgumentNullException exception)
			{ }
		}


		/// <summary>
		/// Вывести значение элемента
		/// </summary>
		private void CircuitTree_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if ((CircuitTree.SelectedNode.Text == @"Capacitor") ||
			    (CircuitTree.SelectedNode.Text == @"Resistor") ||
			    (CircuitTree.SelectedNode.Text == @"Inductor"))
			{
				ValueElementEdit.Text = CircuitTree.SelectedNode.Tag.ToString();
				ChangeValueButton.Enabled = true;
			}
			else
			{
				ChangeValueButton.Enabled = false;
			}
		}


		/// <summary>
		/// Кнопка "ChangeValueButton"
		/// </summary>
		private void ChangeValueButton_Click(object sender, EventArgs e)
		{
			CircuitTree.SelectedNode.Tag = ValueElementEdit.Text;

			CircuitTree_AfterSelect(sender, null);

			_allCircuit = IndetificationCircuit(CircuitTree.TopNode);

			ShowImpedance();
		}


		/// <summary>
		/// Добавление цепи в TreeView
		/// </summary>
		/// <param name="name"> Имя добавляемой цепи </param>
		private void AddCircuitInTree(string name)
		{
			try
			{
				if (CircuitTree.SelectedNode == null)
				{
					if (CircuitTree.GetNodeCount(false) == 0)
					{
						CircuitTree.Nodes.Add(name);
					}
					else
					{
						throw new ApplicationException("You not selected node.");
					}
				}
				else
				{
					if ((CircuitTree.SelectedNode.Text != @"Serial") &&
					   (CircuitTree.SelectedNode.Text != @"Parallel"))
					{
						throw new ApplicationException("Your choice is not correct.");
					}
					else
					{
						CircuitTree.SelectedNode.Nodes.Add(name);
					}
				}
			}
			catch (ApplicationException exception)
			{
				MessageBox.Show(exception.Message);
			}
		}


		/// <summary>
		/// Добавить элемент в TreeView
		/// </summary>
		/// <param name="name"> Имя добавляемого элемента </param>
		private void AddElementInTree(string name)
		{
			ValueElementLabel.Enabled = true;
			ValueElementEdit.Enabled = true;

			ValueElementEdit.Focus();

			try
			{
				if (ValueElementEdit.Text == "")
				{
					throw new ApplicationException("Please, enter value element.");
				}

				if (CircuitTree.SelectedNode == null)
				{
					throw new ApplicationException("You not selected node.");
				}
				else if ((CircuitTree.SelectedNode.Text != @"Serial") &&
						 (CircuitTree.SelectedNode.Text != @"Parallel"))
				{
					throw new ApplicationException("Your choice is not correct.");
				}
				else
				{
					TreeNode newNode = new TreeNode(name);
					newNode.Tag = ValueElementEdit.Text;

					CircuitTree.SelectedNode.Nodes.Add(newNode);
				}
			}
			catch (ApplicationException exception)
			{
				MessageBox.Show(exception.Message);
			}

			DeleteButton.Click -= ActionButton_Click;
			DeleteButton.Click += ActionButton_Click;

			// Очищаем поле ввода значения елемента
			ValueElementEdit.Text = string.Empty;
		}


		/// <summary>
		/// Вывести значение импеданса
		/// </summary>
		/// <param name="component"> Основаная цепь </param>
		private void ShowImpedance(IComponent component)
		{
			Complex impedance = component.CalculateImpedance(Convert.ToDouble(FrequencyEdit.Text));

			ResultImpedanceLabel.Text = '(' + Math.Round(impedance.Real, 3).ToString()
				+ @", " + Math.Round(impedance.Imaginary, 3).ToString() + ')';
		}
		
		
		/// <summary>
		/// Вывести значение импеданса всей цепи
		/// </summary>
		private void ShowImpedance()
		{
			Complex impedance = _allCircuit.CalculateImpedance(Convert.ToDouble(FrequencyEdit.Text));

			ResultImpedanceLabel.Text = '(' + Math.Round(impedance.Real, 3).ToString()
				+ @", " + Math.Round(impedance.Imaginary, 3).ToString() + ')';
		}
	}
}
