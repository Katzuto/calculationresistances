﻿namespace ApplicationCalculationResistances
{
	partial class MainForm
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.MenuBar = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.circuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.firstCircuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.secondCircuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.thirdCircuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fourthCircuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fifthCircuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.DataPanel = new System.Windows.Forms.Panel();
			this.InputDataPanel = new System.Windows.Forms.Panel();
			this.ChangeValueButton = new System.Windows.Forms.Button();
			this.ValueElementEdit = new System.Windows.Forms.MaskedTextBox();
			this.ValueElementLabel = new System.Windows.Forms.Label();
			this.FrequencyEdit = new System.Windows.Forms.MaskedTextBox();
			this.FrequencyLabel = new System.Windows.Forms.Label();
			this.InputDataLabel = new System.Windows.Forms.Label();
			this.CalculateDataPanel = new System.Windows.Forms.Panel();
			this.LabelCalculateData = new System.Windows.Forms.Label();
			this.LabelImpedance = new System.Windows.Forms.Label();
			this.ResultImpedanceLabel = new System.Windows.Forms.Label();
			this.PaintPanel = new System.Windows.Forms.Panel();
			this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.ModelPanels = new System.Windows.Forms.TableLayoutPanel();
			this.TreeDataPanel = new System.Windows.Forms.Panel();
			this.CircuitTree = new System.Windows.Forms.TreeView();
			this.DeleteButton = new System.Windows.Forms.Button();
			this.ActionButton = new System.Windows.Forms.Button();
			this.TopMenuPanel = new System.Windows.Forms.Panel();
			this.InductorButton = new System.Windows.Forms.Button();
			this.ResistanceButton = new System.Windows.Forms.Button();
			this.CapacitorButton = new System.Windows.Forms.Button();
			this.ParallelCircuitButton = new System.Windows.Forms.Button();
			this.SerialCircuitButton = new System.Windows.Forms.Button();
			this.MenuBar.SuspendLayout();
			this.DataPanel.SuspendLayout();
			this.InputDataPanel.SuspendLayout();
			this.CalculateDataPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
			this.ModelPanels.SuspendLayout();
			this.TreeDataPanel.SuspendLayout();
			this.TopMenuPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// MenuBar
			// 
			this.MenuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.circuitToolStripMenuItem});
			this.MenuBar.Location = new System.Drawing.Point(0, 0);
			this.MenuBar.Name = "MenuBar";
			this.MenuBar.Size = new System.Drawing.Size(786, 24);
			this.MenuBar.TabIndex = 0;
			this.MenuBar.Text = "MenuBar";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
			this.saveToolStripMenuItem.Text = "Save";
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
			this.openToolStripMenuItem.Text = "Open";
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			// 
			// circuitToolStripMenuItem
			// 
			this.circuitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.firstCircuitToolStripMenuItem,
            this.secondCircuitToolStripMenuItem,
            this.thirdCircuitToolStripMenuItem,
            this.fourthCircuitToolStripMenuItem,
            this.fifthCircuitToolStripMenuItem});
			this.circuitToolStripMenuItem.Name = "circuitToolStripMenuItem";
			this.circuitToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
			this.circuitToolStripMenuItem.Text = "Circuit";
			// 
			// firstCircuitToolStripMenuItem
			// 
			this.firstCircuitToolStripMenuItem.Name = "firstCircuitToolStripMenuItem";
			this.firstCircuitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.firstCircuitToolStripMenuItem.Text = "First circuit";
			this.firstCircuitToolStripMenuItem.Click += new System.EventHandler(this.firstCircuitToolStripMenuItem_Click);
			// 
			// secondCircuitToolStripMenuItem
			// 
			this.secondCircuitToolStripMenuItem.Name = "secondCircuitToolStripMenuItem";
			this.secondCircuitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.secondCircuitToolStripMenuItem.Text = "Second circuit";
			this.secondCircuitToolStripMenuItem.Click += new System.EventHandler(this.secondCircuitToolStripMenuItem_Click);
			// 
			// thirdCircuitToolStripMenuItem
			// 
			this.thirdCircuitToolStripMenuItem.Name = "thirdCircuitToolStripMenuItem";
			this.thirdCircuitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.thirdCircuitToolStripMenuItem.Text = "Third circuit";
			this.thirdCircuitToolStripMenuItem.Click += new System.EventHandler(this.thirdCircuitToolStripMenuItem_Click);
			// 
			// fourthCircuitToolStripMenuItem
			// 
			this.fourthCircuitToolStripMenuItem.Name = "fourthCircuitToolStripMenuItem";
			this.fourthCircuitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.fourthCircuitToolStripMenuItem.Text = "Fourth circuit";
			this.fourthCircuitToolStripMenuItem.Click += new System.EventHandler(this.fourthCircuitToolStripMenuItem_Click);
			// 
			// fifthCircuitToolStripMenuItem
			// 
			this.fifthCircuitToolStripMenuItem.Name = "fifthCircuitToolStripMenuItem";
			this.fifthCircuitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.fifthCircuitToolStripMenuItem.Text = "Fifth circuit";
			this.fifthCircuitToolStripMenuItem.Click += new System.EventHandler(this.fifthCircuitToolStripMenuItem_Click);
			// 
			// DataPanel
			// 
			this.DataPanel.AutoScroll = true;
			this.DataPanel.BackColor = System.Drawing.SystemColors.HotTrack;
			this.DataPanel.Controls.Add(this.InputDataPanel);
			this.DataPanel.Controls.Add(this.CalculateDataPanel);
			this.DataPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DataPanel.Location = new System.Drawing.Point(551, 3);
			this.DataPanel.Name = "DataPanel";
			this.DataPanel.Size = new System.Drawing.Size(232, 353);
			this.DataPanel.TabIndex = 1;
			// 
			// InputDataPanel
			// 
			this.InputDataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.InputDataPanel.BackColor = System.Drawing.SystemColors.Window;
			this.InputDataPanel.Controls.Add(this.ChangeValueButton);
			this.InputDataPanel.Controls.Add(this.ValueElementEdit);
			this.InputDataPanel.Controls.Add(this.ValueElementLabel);
			this.InputDataPanel.Controls.Add(this.FrequencyEdit);
			this.InputDataPanel.Controls.Add(this.FrequencyLabel);
			this.InputDataPanel.Controls.Add(this.InputDataLabel);
			this.InputDataPanel.Location = new System.Drawing.Point(12, 12);
			this.InputDataPanel.Name = "InputDataPanel";
			this.InputDataPanel.Size = new System.Drawing.Size(206, 181);
			this.InputDataPanel.TabIndex = 8;
			// 
			// ChangeValueButton
			// 
			this.ChangeValueButton.Enabled = false;
			this.ChangeValueButton.Location = new System.Drawing.Point(65, 108);
			this.ChangeValueButton.Name = "ChangeValueButton";
			this.ChangeValueButton.Size = new System.Drawing.Size(115, 26);
			this.ChangeValueButton.TabIndex = 11;
			this.ChangeValueButton.Text = "Change value";
			this.ChangeValueButton.UseVisualStyleBackColor = true;
			this.ChangeValueButton.Click += new System.EventHandler(this.ChangeValueButton_Click);
			// 
			// ValueElementEdit
			// 
			this.ValueElementEdit.Location = new System.Drawing.Point(113, 72);
			this.ValueElementEdit.Name = "ValueElementEdit";
			this.ValueElementEdit.Size = new System.Drawing.Size(67, 20);
			this.ValueElementEdit.TabIndex = 10;
			this.ValueElementEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValueElementEdit_KeyPress);
			// 
			// ValueElementLabel
			// 
			this.ValueElementLabel.AutoSize = true;
			this.ValueElementLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ValueElementLabel.Location = new System.Drawing.Point(18, 72);
			this.ValueElementLabel.Name = "ValueElementLabel";
			this.ValueElementLabel.Size = new System.Drawing.Size(56, 16);
			this.ValueElementLabel.TabIndex = 8;
			this.ValueElementLabel.Text = "Value = ";
			// 
			// FrequencyEdit
			// 
			this.FrequencyEdit.Location = new System.Drawing.Point(113, 39);
			this.FrequencyEdit.Name = "FrequencyEdit";
			this.FrequencyEdit.Size = new System.Drawing.Size(67, 20);
			this.FrequencyEdit.TabIndex = 6;
			this.FrequencyEdit.Text = "10";
			this.FrequencyEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrequencyEdit_KeyPress);
			// 
			// FrequencyLabel
			// 
			this.FrequencyLabel.AutoSize = true;
			this.FrequencyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.FrequencyLabel.Location = new System.Drawing.Point(18, 39);
			this.FrequencyLabel.Name = "FrequencyLabel";
			this.FrequencyLabel.Size = new System.Drawing.Size(82, 16);
			this.FrequencyLabel.TabIndex = 3;
			this.FrequencyLabel.Text = "Frequency =";
			// 
			// InputDataLabel
			// 
			this.InputDataLabel.AutoSize = true;
			this.InputDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.InputDataLabel.Location = new System.Drawing.Point(62, 10);
			this.InputDataLabel.Name = "InputDataLabel";
			this.InputDataLabel.Size = new System.Drawing.Size(74, 18);
			this.InputDataLabel.TabIndex = 2;
			this.InputDataLabel.Text = "Input Data";
			// 
			// CalculateDataPanel
			// 
			this.CalculateDataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CalculateDataPanel.AutoScroll = true;
			this.CalculateDataPanel.BackColor = System.Drawing.SystemColors.Window;
			this.CalculateDataPanel.Controls.Add(this.LabelCalculateData);
			this.CalculateDataPanel.Controls.Add(this.LabelImpedance);
			this.CalculateDataPanel.Controls.Add(this.ResultImpedanceLabel);
			this.CalculateDataPanel.Location = new System.Drawing.Point(12, 199);
			this.CalculateDataPanel.Name = "CalculateDataPanel";
			this.CalculateDataPanel.Size = new System.Drawing.Size(206, 148);
			this.CalculateDataPanel.TabIndex = 7;
			// 
			// LabelCalculateData
			// 
			this.LabelCalculateData.AutoSize = true;
			this.LabelCalculateData.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LabelCalculateData.Location = new System.Drawing.Point(45, 10);
			this.LabelCalculateData.Name = "LabelCalculateData";
			this.LabelCalculateData.Size = new System.Drawing.Size(109, 18);
			this.LabelCalculateData.TabIndex = 1;
			this.LabelCalculateData.Text = "Calculated data";
			// 
			// LabelImpedance
			// 
			this.LabelImpedance.AutoSize = true;
			this.LabelImpedance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LabelImpedance.Location = new System.Drawing.Point(18, 37);
			this.LabelImpedance.Name = "LabelImpedance";
			this.LabelImpedance.Size = new System.Drawing.Size(79, 16);
			this.LabelImpedance.TabIndex = 0;
			this.LabelImpedance.Text = "Impedance:";
			// 
			// ResultImpedanceLabel
			// 
			this.ResultImpedanceLabel.AutoSize = true;
			this.ResultImpedanceLabel.Location = new System.Drawing.Point(103, 39);
			this.ResultImpedanceLabel.Name = "ResultImpedanceLabel";
			this.ResultImpedanceLabel.Size = new System.Drawing.Size(29, 13);
			this.ResultImpedanceLabel.TabIndex = 5;
			this.ResultImpedanceLabel.Text = "NaN";
			// 
			// PaintPanel
			// 
			this.PaintPanel.AutoScroll = true;
			this.PaintPanel.AutoScrollMinSize = new System.Drawing.Size(30, 30);
			this.PaintPanel.AutoSize = true;
			this.PaintPanel.BackColor = System.Drawing.SystemColors.Window;
			this.PaintPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.PaintPanel.Location = new System.Drawing.Point(162, 3);
			this.PaintPanel.Name = "PaintPanel";
			this.PaintPanel.Size = new System.Drawing.Size(383, 353);
			this.PaintPanel.TabIndex = 3;
			// 
			// ModelPanels
			// 
			this.ModelPanels.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ModelPanels.BackColor = System.Drawing.SystemColors.HotTrack;
			this.ModelPanels.ColumnCount = 3;
			this.ModelPanels.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.00901F));
			this.ModelPanels.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.99099F));
			this.ModelPanels.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237F));
			this.ModelPanels.Controls.Add(this.DataPanel, 2, 0);
			this.ModelPanels.Controls.Add(this.PaintPanel, 1, 0);
			this.ModelPanels.Controls.Add(this.TreeDataPanel, 0, 0);
			this.ModelPanels.Location = new System.Drawing.Point(0, 63);
			this.ModelPanels.Margin = new System.Windows.Forms.Padding(0);
			this.ModelPanels.Name = "ModelPanels";
			this.ModelPanels.RowCount = 1;
			this.ModelPanels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37F));
			this.ModelPanels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 359F));
			this.ModelPanels.Size = new System.Drawing.Size(786, 359);
			this.ModelPanels.TabIndex = 8;
			// 
			// TreeDataPanel
			// 
			this.TreeDataPanel.BackColor = System.Drawing.SystemColors.HotTrack;
			this.TreeDataPanel.Controls.Add(this.CircuitTree);
			this.TreeDataPanel.Controls.Add(this.DeleteButton);
			this.TreeDataPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TreeDataPanel.Location = new System.Drawing.Point(3, 3);
			this.TreeDataPanel.Name = "TreeDataPanel";
			this.TreeDataPanel.Size = new System.Drawing.Size(153, 353);
			this.TreeDataPanel.TabIndex = 4;
			// 
			// CircuitTree
			// 
			this.CircuitTree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CircuitTree.Location = new System.Drawing.Point(10, 14);
			this.CircuitTree.Name = "CircuitTree";
			this.CircuitTree.Size = new System.Drawing.Size(134, 289);
			this.CircuitTree.TabIndex = 0;
			this.CircuitTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.CircuitTree_AfterSelect);
			// 
			// DeleteButton
			// 
			this.DeleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DeleteButton.Location = new System.Drawing.Point(9, 315);
			this.DeleteButton.Name = "DeleteButton";
			this.DeleteButton.Size = new System.Drawing.Size(135, 30);
			this.DeleteButton.TabIndex = 5;
			this.DeleteButton.Text = "Delete";
			this.DeleteButton.UseVisualStyleBackColor = true;
			this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
			// 
			// ActionButton
			// 
			this.ActionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.ActionButton.Location = new System.Drawing.Point(586, 4);
			this.ActionButton.Name = "ActionButton";
			this.ActionButton.Size = new System.Drawing.Size(134, 30);
			this.ActionButton.TabIndex = 1;
			this.ActionButton.Text = "Action";
			this.ActionButton.UseVisualStyleBackColor = true;
			this.ActionButton.Visible = false;
			this.ActionButton.Click += new System.EventHandler(this.ActionButton_Click);
			// 
			// TopMenuPanel
			// 
			this.TopMenuPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TopMenuPanel.BackColor = System.Drawing.SystemColors.Control;
			this.TopMenuPanel.Controls.Add(this.ActionButton);
			this.TopMenuPanel.Controls.Add(this.InductorButton);
			this.TopMenuPanel.Controls.Add(this.ResistanceButton);
			this.TopMenuPanel.Controls.Add(this.CapacitorButton);
			this.TopMenuPanel.Controls.Add(this.ParallelCircuitButton);
			this.TopMenuPanel.Controls.Add(this.SerialCircuitButton);
			this.TopMenuPanel.Location = new System.Drawing.Point(0, 24);
			this.TopMenuPanel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.TopMenuPanel.Name = "TopMenuPanel";
			this.TopMenuPanel.Size = new System.Drawing.Size(786, 40);
			this.TopMenuPanel.TabIndex = 9;
			// 
			// InductorButton
			// 
			this.InductorButton.Location = new System.Drawing.Point(370, 3);
			this.InductorButton.Name = "InductorButton";
			this.InductorButton.Size = new System.Drawing.Size(75, 30);
			this.InductorButton.TabIndex = 4;
			this.InductorButton.Text = "Inductor";
			this.InductorButton.UseVisualStyleBackColor = true;
			this.InductorButton.Click += new System.EventHandler(this.InductorButton_Click);
			// 
			// ResistanceButton
			// 
			this.ResistanceButton.Location = new System.Drawing.Point(288, 3);
			this.ResistanceButton.Name = "ResistanceButton";
			this.ResistanceButton.Size = new System.Drawing.Size(75, 30);
			this.ResistanceButton.TabIndex = 3;
			this.ResistanceButton.Text = "Resistance";
			this.ResistanceButton.UseVisualStyleBackColor = true;
			this.ResistanceButton.Click += new System.EventHandler(this.ResistanceButton_Click);
			// 
			// CapacitorButton
			// 
			this.CapacitorButton.Location = new System.Drawing.Point(206, 3);
			this.CapacitorButton.Name = "CapacitorButton";
			this.CapacitorButton.Size = new System.Drawing.Size(75, 30);
			this.CapacitorButton.TabIndex = 2;
			this.CapacitorButton.Text = "Capacitor";
			this.CapacitorButton.UseVisualStyleBackColor = true;
			this.CapacitorButton.Click += new System.EventHandler(this.CapacitorButton_Click);
			// 
			// ParallelCircuitButton
			// 
			this.ParallelCircuitButton.Location = new System.Drawing.Point(86, 4);
			this.ParallelCircuitButton.Name = "ParallelCircuitButton";
			this.ParallelCircuitButton.Size = new System.Drawing.Size(75, 30);
			this.ParallelCircuitButton.TabIndex = 1;
			this.ParallelCircuitButton.Text = "Parallel";
			this.ParallelCircuitButton.UseVisualStyleBackColor = true;
			this.ParallelCircuitButton.Click += new System.EventHandler(this.ParallelCircuitButton_Click);
			// 
			// SerialCircuitButton
			// 
			this.SerialCircuitButton.Location = new System.Drawing.Point(4, 4);
			this.SerialCircuitButton.Name = "SerialCircuitButton";
			this.SerialCircuitButton.Size = new System.Drawing.Size(75, 30);
			this.SerialCircuitButton.TabIndex = 0;
			this.SerialCircuitButton.Text = "Serial";
			this.SerialCircuitButton.UseVisualStyleBackColor = true;
			this.SerialCircuitButton.Click += new System.EventHandler(this.SerialCircuitButton_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(786, 423);
			this.Controls.Add(this.TopMenuPanel);
			this.Controls.Add(this.ModelPanels);
			this.Controls.Add(this.MenuBar);
			this.MainMenuStrip = this.MenuBar;
			this.MinimumSize = new System.Drawing.Size(802, 462);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Calculation Resistances";
			this.MenuBar.ResumeLayout(false);
			this.MenuBar.PerformLayout();
			this.DataPanel.ResumeLayout(false);
			this.InputDataPanel.ResumeLayout(false);
			this.InputDataPanel.PerformLayout();
			this.CalculateDataPanel.ResumeLayout(false);
			this.CalculateDataPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
			this.ModelPanels.ResumeLayout(false);
			this.ModelPanels.PerformLayout();
			this.TreeDataPanel.ResumeLayout(false);
			this.TopMenuPanel.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip MenuBar;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem circuitToolStripMenuItem;
		private System.Windows.Forms.Panel DataPanel;
		private System.Windows.Forms.Panel PaintPanel;
		private System.Windows.Forms.ToolStripMenuItem firstCircuitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem secondCircuitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem thirdCircuitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fourthCircuitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fifthCircuitToolStripMenuItem;
		private System.Windows.Forms.Label LabelImpedance;
		private System.Windows.Forms.Label LabelCalculateData;
		private System.Windows.Forms.BindingSource bindingSource1;
		private System.Windows.Forms.Label InputDataLabel;
		private System.Windows.Forms.Label FrequencyLabel;
		private System.Windows.Forms.Label ResultImpedanceLabel;
		private System.Windows.Forms.MaskedTextBox FrequencyEdit;
		private System.Windows.Forms.Panel InputDataPanel;
		private System.Windows.Forms.Panel CalculateDataPanel;
		private System.Windows.Forms.Label ValueElementLabel;
		private System.Windows.Forms.MaskedTextBox ValueElementEdit;
		private System.Windows.Forms.TableLayoutPanel ModelPanels;
		private System.Windows.Forms.Panel TopMenuPanel;
		private System.Windows.Forms.Panel TreeDataPanel;
		private System.Windows.Forms.Button ActionButton;
		private System.Windows.Forms.TreeView CircuitTree;
		private System.Windows.Forms.Button InductorButton;
		private System.Windows.Forms.Button ResistanceButton;
		private System.Windows.Forms.Button CapacitorButton;
		private System.Windows.Forms.Button ParallelCircuitButton;
		private System.Windows.Forms.Button SerialCircuitButton;
		private System.Windows.Forms.Button DeleteButton;
		private System.Windows.Forms.Button ChangeValueButton;
	}
}

