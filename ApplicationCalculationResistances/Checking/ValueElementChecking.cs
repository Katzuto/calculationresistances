﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Класс проверки значения елементов
	/// </summary>
	public static class ValueElementChecking
	{
		/// <summary>
		/// Код кнопки 'Backspace' - 8
		/// </summary>
		private const int ButtonBackspace = 8;


		/// <summary>
		/// Ограничение нажатие клавиш, кроме цифр, ',' и 'Backspace'
		/// </summary>
		/// <param name="keyPress"> Нажатая клавиша </param>
		public static void CheckOnlyNumbers(KeyPressEventArgs keyPress)
		{
			if (!((keyPress.KeyChar >= '0') && (keyPress.KeyChar <= '9')
				  || (keyPress.KeyChar == ',') || (keyPress.KeyChar == ButtonBackspace)))
			{
				keyPress.KeyChar = '\0';
			}
		}

	}
}
