﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Проверка для элемента TreeView
	/// </summary>
	public static class TreeViewChecking
	{
		/// <summary>
		/// Проверка на наличие элементов в цепи
		/// </summary>
		/// <param name="treeNode"> Первые элемент цепи </param>
		public static bool CheckElementTreeView(TreeNode treeNode)
		{
			List<bool> availabilityElement = new List<bool>();

			switch (treeNode.Text)
			{
				case "Serial":
					foreach (TreeNode tnn in treeNode.Nodes)
					{
						availabilityElement.Add(CheckElementTreeView(tnn));
					}
					break;
				case "Parallel":
					foreach (TreeNode tnn in treeNode.Nodes)
					{
						availabilityElement.Add(CheckElementTreeView(tnn));
					}
					break;
				case "Resistor":
					return true;
					break;
				case "Capacitor":
					return true;
					break;
				case "Inductor":
					return true;
					break;
				default:
					return false;
					break;
			}

			for (int i = 0; i < availabilityElement.Count; ++i)
			{
				if (availabilityElement[i] == true)
				{
					return true;
				}
			}

			return false;
		}
	}
}
