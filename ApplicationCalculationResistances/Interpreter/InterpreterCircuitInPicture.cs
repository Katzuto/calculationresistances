﻿using System;
using ModelCalculationResistances;

namespace ApplicationCalculationResistances.Interpreter
{
    //TODO: static - не очень удачно, можно создавать экземпляр и передавать какие-нибудь настройки, например кисть для рисования и заливку канвы
	/// <summary>
	/// Ретранслятор цепи (IComponent) в изображение (IComponentDrawer)
	/// </summary>
	public static class InterpreterCircuitInPicture
	{
		/// <summary>
		/// Функция форматирующая формат IComponent в IComponentDrawer
		/// </summary>
		/// <param name="component"> Входные данные </param>
		public static IComponentDrawer Interpreter(IComponent component)
		{
			IComponentDrawer componentDrawer = null;

			if (component is ICircuit)
			{
				if (component is ParallelCircuit)
				{
					componentDrawer = new ParallelCircuitDrawer();
				}
				else if (component is SerialCircuit)
				{
					componentDrawer = new SerialCircuitDrawer();
				}

				for (int i = 0; i < ((BaseCircuit)component).GetCountItems(); ++i)
				{
					if (componentDrawer != null)
					{
						componentDrawer.Add(Interpreter(((BaseCircuit)component).GetItem(i)));
					}
				}
			}

			if (component is IElement)
			{
				if (component is Capacitor)
				{
					return new CapacitorDrawer();
				}

				if (component is Resistor)
				{
					return new ResistorDrawer();
				}

				if (component is Inductor)
				{
					return new InductorDrawer();
				}
			}

			if (componentDrawer == null)
			{
				throw new ApplicationException("Not known element.");
			}

			return componentDrawer;
		}
	}
}
