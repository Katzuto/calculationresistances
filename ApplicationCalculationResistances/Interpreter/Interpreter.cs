﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelCalculationResistances;

namespace ApplicationCalculationResistances.Interpreter
{
	/// <summary>
	/// Класс интепритации типов
	/// </summary>
	public static class Interpreter
	{
		/// <summary>
		/// Функция форматирующая формат IComponent в IComponentDrawer
		/// </summary>
		/// <param name="component"> Входные данные </param>
		public static IComponentDrawer InterpreterCircuitInPicture(IComponent component)
		{
			IComponentDrawer componentDrawer = null;

			if (component is ICircuit)
			{
				if (component is ParallelCircuit)
				{
					componentDrawer = new ParallelCircuitDrawer();
				}
				else if (component is SerialCircuit)
				{
					componentDrawer = new SerialCircuitDrawer();
				}

				for (int i = 0; i < ((BaseCircuit)component).GetCountItems(); ++i)
				{
					if (componentDrawer != null)
					{
						componentDrawer.Add(InterpreterCircuitInPicture(((BaseCircuit)component).GetItem(i)));
					}
				}
			}

			if (component is IElement)
			{
				if (component is Capacitor)
				{
					return new CapacitorDrawer();
				}

				if (component is Resistor)
				{
					return new ResistorDrawer();
				}

				if (component is Inductor)
				{
					return new InductorDrawer();
				}
			}

			if (componentDrawer == null)
			{
				throw new ApplicationException("Not known element.");
			}

			return componentDrawer;
		}


		/// <summary>
		/// Функция интерпретирующая IComponent в TreeNode
		/// </summary>
		/// <param name="component"> Входные данные </param>
		public static TreeNode InterpreterCircuitInTreeNode(IComponent component)
		{
			string name = "NaN";

			if (component is IElement)
			{
				name = ((IElement)component).Name;
				TreeNode newNode = new TreeNode(name);
				newNode.Tag = ((IElement)component).ValueElement.ToString();
				return newNode;
			}

			if (component is ICircuit)
			{
				if (component is ParallelCircuit)
				{
					name = "Parallel";

					TreeNode newNode = new TreeNode(name);

					for (int i = 0; i < ((BaseCircuit) component).GetCountItems(); ++i)
					{
						newNode.Nodes.Add(InterpreterCircuitInTreeNode
							(((BaseCircuit) component).GetItem(i)));
					}

					return newNode;
				}

				if (component is SerialCircuit)
				{
					name = "Serial";

					TreeNode newNode = new TreeNode(name);

					for (int i = 0; i < ((BaseCircuit)component).GetCountItems(); ++i)
					{
						newNode.Nodes.Add(InterpreterCircuitInTreeNode
							(((BaseCircuit)component).GetItem(i)));
					}

					return newNode;
				}
			}

			return null;
		}

	}
}
