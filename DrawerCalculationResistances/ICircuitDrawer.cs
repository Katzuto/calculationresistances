﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelCalculationResistances;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Интерфейс отрисовки цепей
	/// </summary>
	public interface ICircuitDrawer : IComponentDrawer
	{
		/// <summary>
		/// Метод отрисовки цепи
		/// </summary>
		/// <param name="component"> Входная цепь </param>
		/// <returns> Bitmap с отрисованной цепью </returns>
		Bitmap DrawComponent();
	}
}
