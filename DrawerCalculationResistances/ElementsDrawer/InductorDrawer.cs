﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Класс отрисовщик катушки индуктивности
	/// </summary>
	public class InductorDrawer : BaseElementDrawer
	{
		/// <summary>
		/// Метод отрисовки катушки индуктивности
		/// </summary>
		/// <returns> Bitmap содержащий отрисованную катушку индуктивности </returns>
		public override Bitmap DrawComponent()
		{
			Bitmap inductorBitmap = new Bitmap(40, 40);

			Graphics myGraphics = Graphics.FromImage(inductorBitmap);

			Pen myPen = new Pen(Color.Black, 2);

			float x = 0;    // Координата x
			float y = 20;   // Координата y

			myGraphics.DrawLine(myPen, x, y, x + 10F, y);

			for (int i = 0, temp = 5; i < 4; ++i)
			{
				myGraphics.DrawArc(myPen, (x + 10F + i * temp), (y - 7.5F), 5, 15, 0, -180);
			}

			myGraphics.DrawLine(myPen, x + 30F, y, x + 40F, y);

			myPen.Dispose();
			myGraphics.Dispose();

			return inductorBitmap;
		}
	}
}
