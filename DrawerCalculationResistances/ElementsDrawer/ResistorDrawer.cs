﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Класс отрисовщик резистора
	/// </summary>
	public class ResistorDrawer : BaseElementDrawer
	{
		/// <summary>
		/// Метод отрисовки резистора
		/// </summary>
		/// <returns> Bitmap содержащий отрисованный конденсатор </returns>
		public override Bitmap DrawComponent()
		{
			Bitmap resistiorBitmap = new Bitmap(40, 40);

			Graphics myGraphics = Graphics.FromImage(resistiorBitmap);

			Pen myPen = new Pen(Color.Black, 2);

			float x = 0;    // Координата x
			float y = 20;   // Координата y

			myGraphics.DrawLine(myPen, x, y, x + 10F, y);

			myGraphics.DrawRectangle(myPen, new System.Drawing.Rectangle((int)(x + 10F), (int)(y - 5F), 20, 10));

			myGraphics.DrawLine(myPen, x + 30F, y, x + 40F, y);

			myPen.Dispose();
			myGraphics.Dispose();

			return resistiorBitmap;
		}
	}
}
