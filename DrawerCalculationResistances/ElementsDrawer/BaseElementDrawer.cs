﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCalculationResistances
{
	public abstract class BaseElementDrawer : IElementDrawer
	{
		/// <summary>
		/// Метод добавления элементов в цепь
		/// </summary>
		/// <param name="component"> Добавляемый компонент </param>
		public void Add(IComponentDrawer component)
		{
			throw new NotImplementedException();
		}


		/// <summary>
		/// Метод удаления элементов из цепи
		/// </summary>
		/// <param name="component"> Удаляемый компонент </param>
		public void Remove(IComponentDrawer component)
		{
			throw new NotImplementedException();
		}


		/// <summary>
		/// Метод отрисовки элемента
		/// </summary>
		public abstract Bitmap DrawComponent();
	}
}
