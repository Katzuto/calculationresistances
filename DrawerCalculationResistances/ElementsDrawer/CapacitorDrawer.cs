﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelCalculationResistances;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Класс отрисовщик конденсатора
	/// </summary>
	public class CapacitorDrawer : BaseElementDrawer
	{
		/// <summary>
		/// Метод отрисовки конденсатора
		/// </summary>
		/// /// <returns> Bitmap содержащий отрисованный резистор </returns>
		public override Bitmap DrawComponent()
		{
			Bitmap capacitorBitmap = new Bitmap(40, 40);

			Graphics myGraphics = Graphics.FromImage(capacitorBitmap);

			Pen myPen = new Pen(Color.Black, 2);

			float x = 0;    // Координата x
			float y = 20;   // Координата y

			myGraphics.DrawLine(myPen, x, y, x + 17.5F, y);

			for (int i = 0, temp = 5; i < 2; ++i)
			{
				myGraphics.DrawLine(myPen, (x + 17.5F + i * temp), (y - 10F), (x + 17.5F + i * temp), (y + 10F));
			}

			myGraphics.DrawLine(myPen, x + 22.5F, y, x + 40F, y);

			myPen.Dispose();
			myGraphics.Dispose();

			return capacitorBitmap;
		}
	}
}
