﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Класс интерфейс для элементов отрисовки
	/// </summary>
	public interface IElementDrawer : IComponentDrawer
	{
		/// <summary>
		/// Метод отрисовки элемента
		/// </summary>
		/// <returns> Bitmap с отрисованным элементом </returns>
		Bitmap DrawComponent();
	}
}
