﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelCalculationResistances;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Интерфейс компонента отрисовщика
	/// </summary>
	public interface IComponentDrawer
	{
		/// <summary>
		/// Метод отрисовки компонента
		/// </summary>
		/// <param name="component"> Входной компонент </param>
		/// <returns> Bitmap с отрисованным компонентом </returns>
		Bitmap DrawComponent();


		/// <summary>
		/// Метод добавления компонентов
		/// </summary>
		/// <param name="component"> Добавляемый компонент </param>
		void Add(IComponentDrawer component);


		/// <summary>
		/// Метод удаления компонентов
		/// </summary>
		/// <param name="component"> Удаляемый компонент </param>
		void Remove(IComponentDrawer component);
	}
}
