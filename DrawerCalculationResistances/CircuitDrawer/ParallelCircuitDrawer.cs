﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawerCalculationResistances;
using ModelCalculationResistances;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Класс отрисовки параллельного соединения цепи
	/// </summary>
	public class ParallelCircuitDrawer : BaseCircuitDrawer
	{
		/// <summary>
		/// Метод отрисовки параллельной цепи
		/// </summary>
		/// <returns> Bitmap с отрисованной параллельной цепью </returns>
		public override Bitmap DrawComponent()
		{
			// Список для хранения всех Bitmap'ов в цепи
			List<Bitmap> elements = new List<Bitmap>();

			// Получаем Bitmap'ы каждого элемента цепи
			for (int i = 0; i < this.Component.Count; ++i)
			{
				elements.Add(Component[i].DrawComponent());
			}

			int maxWidth = 0;      // Самая большая ширина картинки
			int allHeight = 0;

			if (elements.Count != 0)    // Новая функциональность
			{
				for (int i = 0; i < elements.Count; ++i)
				{
					if (elements[i] != null) // Вот это может быть надо убрать
					{
						// Находим максимальное значение ширины среди элементов цепи
						maxWidth = (maxWidth < elements[i].Width) ? elements[i].Width : maxWidth;

						allHeight += elements[i].Height;
					}
				}

				// Создаем Bitmap всех элементов цепи
				Bitmap circuitBitmap = new Bitmap(maxWidth + 40, allHeight);

				Graphics myGraphics = Graphics.FromImage(circuitBitmap);

				float y = 0;

				for (int i = 0; i < elements.Count; ++i)
				{
					if (elements[i] != null)
					{
						float x = ((circuitBitmap.Width) / 2) - (elements[i].Width / 2);

						myGraphics.DrawImage(elements[i], x, y);

						y += elements[i].Height;
					}
				}

				Pen myPen = new Pen(Color.Black, 2);

				myGraphics.DrawLine(myPen, 0, (circuitBitmap.Height/2), 10,
					(circuitBitmap.Height/2));
				myGraphics.DrawLine(myPen, 10, (elements[0].Height/2), 10,
					circuitBitmap.Height - (elements[elements.Count - 1].Height/2));

				float tempHeight = 0;

				for (int i = 0; i < elements.Count; ++i)
				{
					tempHeight += elements[i].Height;

					myGraphics.DrawLine(myPen, 10, tempHeight - (elements[i].Height/2),
						(circuitBitmap.Width/2) - (elements[i].Width/2), tempHeight - (elements[i].Height/2));

					myGraphics.DrawLine(myPen, (circuitBitmap.Width/2) + (elements[i].Width/2), tempHeight - (elements[i].Height/2),
						circuitBitmap.Width - 10, tempHeight - (elements[i].Height/2));
				}

				myGraphics.DrawLine(myPen, (circuitBitmap.Width - 10), (elements[0].Height/2),
					(circuitBitmap.Width - 10), circuitBitmap.Height - (elements[elements.Count - 1].Height/2));
				myGraphics.DrawLine(myPen, (circuitBitmap.Width - 10), (circuitBitmap.Height/2),
					circuitBitmap.Width, (circuitBitmap.Height/2));

				return circuitBitmap;
			}

			return null;
		}
	}
}
