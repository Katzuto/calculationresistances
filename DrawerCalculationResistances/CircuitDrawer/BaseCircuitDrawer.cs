﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationCalculationResistances;
using ModelCalculationResistances;

namespace DrawerCalculationResistances
{
	public abstract class BaseCircuitDrawer : ICircuitDrawer
	{
		/// <summary>
		/// Компоненты(цепи, элементы) в цепи для отрисовки
		/// </summary>
		protected readonly List<IComponentDrawer> Component = new List<IComponentDrawer>();


		/// <summary>
		/// Метод добавления компонентов(цепи, элементов) в цепь
		/// </summary>
		/// <param name="component"> Добавляемый компонент </param>
		public void Add(IComponentDrawer component)
		{
			Component.Add(component);
		}


		/// <summary>
		/// Метод удаления компонентов(цепи, элементов) в цепь
		/// </summary>
		/// <param name="component"> Удалеяемый компонент </param>
		public void Remove(IComponentDrawer component)
		{
			Component.Remove(component);
		}


		/// <summary>
		/// Метод отрисовки параллельной цепи
		/// </summary>
		public abstract Bitmap DrawComponent();
	}
}
