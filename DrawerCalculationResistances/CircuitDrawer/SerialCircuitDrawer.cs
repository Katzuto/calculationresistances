﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawerCalculationResistances;
using ModelCalculationResistances;

namespace ApplicationCalculationResistances
{
	/// <summary>
	/// Класс отрисовки последовательного соединения цепи
	/// </summary>
	public class SerialCircuitDrawer : BaseCircuitDrawer
	{
		/// <summary>
		/// Метод отрисовки последовательной цепи
		/// </summary>
		/// <returns> Bitmap с отрисованной последовательной цепью </returns>
		public override Bitmap DrawComponent()
		{
			//var serialCircuit = new SerialCircuitDrawer((BaseCircuit) Component);

			// Список для хранения всех Bitmap'ов в цепи
			List<Bitmap> elements = new List<Bitmap>();

			// Получаем Bitmap'ы каждого элемента цепи
			for (int i = 0; i < Component.Count; ++i)
			{
				elements.Add(Component[i].DrawComponent());
			}

			int maxHeight = 0;     // Самая большая высота картинки
			int allWidth = 0;

			if (elements.Count != 0)    // Новая функциональность
			{

				for (int i = 0; i < elements.Count; ++i)
				{
					if (elements[i] != null)	// Вот это может быть надо убрать
					{
						// Наодим минимальное значение высоты среди элементов цепи
						maxHeight = (maxHeight < elements[i].Height) ? elements[i].Height : maxHeight;

						allWidth += elements[i].Width;
					}
				}
			
				// Создаем Bitmap всех элементов цепи
				Bitmap circuitBitmap = new Bitmap(allWidth, maxHeight);

				Graphics myGraphics = Graphics.FromImage(circuitBitmap);

				float x = 0;

				for (int i = 0; i < elements.Count; ++i)
				{
					if (elements[i] != null) 
					{
						float y = (maxHeight - elements[i].Height) / 2;

						myGraphics.DrawImage(elements[i], x, y);

						x += elements[i].Width;
					}
				}

				return circuitBitmap;
			}

			return null;
		}
	}
}
