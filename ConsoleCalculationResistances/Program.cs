﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using ModelCalculationResistances;

namespace ConsoleCalculationResistances
{
	class Program
	{
		static void Main()
		{
			// Первая схема
			IComponent CircuitFirst = new SerialCircuit();

			IComponent ElementResistor = new Resistor(5D);

			IComponent CircuitSecond = new ParallelCircuit();

			IComponent ElementCapacitor = new Capacitor(10D);

			IComponent ElementInductor = new Inductor(15D);

			CircuitSecond.Add(ElementInductor);
			CircuitSecond.Add(ElementCapacitor);
			
			CircuitFirst.Add(ElementResistor);
			CircuitFirst.Add(CircuitSecond);

			Console.WriteLine(((IElement) ElementResistor).ValueElement);
			Console.WriteLine(((IElement) ElementResistor).Name);
			Console.WriteLine();

			Console.WriteLine(((IElement)ElementCapacitor).Name);
			Console.WriteLine(((IElement)ElementCapacitor).ValueElement);
			Console.WriteLine();

			Console.WriteLine(((IElement)ElementInductor).Name);
			Console.WriteLine(((IElement)ElementInductor).ValueElement);
			Console.WriteLine();

			Console.WriteLine("Схема первая: ");
			Console.WriteLine("Импеданс x + i*y: " + ((ICircuit) CircuitFirst).CalculateImpedance(20D));
			Console.WriteLine();


			// Вторая схема
			CircuitFirst = new SerialCircuit();

			CircuitSecond = new ParallelCircuit();

			ElementResistor = new Resistor(5D);

			ElementCapacitor = new Capacitor(8D);

			ElementInductor = new Inductor(12D);

			CircuitSecond.Add(ElementResistor);
			CircuitSecond.Add(ElementCapacitor);

			CircuitFirst.Add(CircuitSecond);
			CircuitFirst.Add(ElementInductor);

			Console.WriteLine("Схема вторая: ");
			Console.WriteLine("Импеданс x + i*y: " + ((ICircuit)CircuitFirst).CalculateImpedance(20D));
			Console.WriteLine();


			// Третья схема
			CircuitFirst = new SerialCircuit();

			CircuitSecond = new ParallelCircuit();

			IComponent CircuitThird = new ParallelCircuit();

			ElementResistor = new Resistor(5D);

			ElementCapacitor = new Capacitor(8D);

			ElementInductor = new Inductor(12D);

			CircuitSecond.Add(ElementCapacitor);
			CircuitSecond.Add(ElementInductor);

			CircuitThird.Add(ElementInductor);
			CircuitThird.Add(ElementCapacitor);

			CircuitFirst.Add(CircuitSecond);
			CircuitFirst.Add(ElementInductor);
			CircuitFirst.Add(CircuitThird);

			Console.WriteLine("Схема третья: ");
			Console.WriteLine("Импеданс x + i*y: " + ((ICircuit)CircuitFirst).CalculateImpedance(20D));
			Console.WriteLine();


			// Четвертая схема
			CircuitFirst = new ParallelCircuit();

			CircuitSecond = new SerialCircuit();

			CircuitThird = new ParallelCircuit();

			IComponent CircuitFourth = new SerialCircuit();

			IComponent CircuitFifth = new ParallelCircuit();

			ElementResistor = new Resistor(5D);

			ElementCapacitor = new Capacitor(8D);

			ElementInductor = new Inductor(12D);

			CircuitThird.Add(ElementResistor);
			CircuitThird.Add(ElementInductor);

			CircuitSecond.Add(ElementCapacitor);
			CircuitSecond.Add(CircuitThird);

			CircuitFifth.Add(ElementInductor);
			CircuitFifth.Add(ElementResistor);

			CircuitFourth.Add(CircuitFifth);
			CircuitFourth.Add(ElementCapacitor);

			CircuitFirst.Add(CircuitSecond);
			CircuitFirst.Add(CircuitFourth);

			Console.WriteLine("Схема четыре: ");
			Console.WriteLine("Импеданс x + i*y: " + ((ICircuit)CircuitFirst).CalculateImpedance(20D));
			Console.WriteLine();


			// Пятая схема
			CircuitFirst = new SerialCircuit();

			CircuitSecond = new ParallelCircuit();

			CircuitThird = new SerialCircuit();

			CircuitFourth = new ParallelCircuit();

			CircuitFifth = new SerialCircuit();

			ElementResistor = new Resistor(5D);

			ElementCapacitor = new Capacitor(8D);

			ElementInductor = new Inductor(12D);

			CircuitFourth.Add(ElementCapacitor);
			CircuitFourth.Add(ElementResistor);

			CircuitThird.Add(ElementInductor);
			CircuitThird.Add(CircuitFourth);

			CircuitFifth.Add(ElementResistor);
			CircuitFifth.Add(ElementInductor);

			CircuitSecond.Add(ElementInductor);
			CircuitSecond.Add(CircuitThird);
			CircuitSecond.Add(CircuitFifth);

			CircuitFirst.Add(ElementInductor);
			CircuitFirst.Add(CircuitSecond);

			Console.WriteLine("Схема пятая: ");
			Console.WriteLine("Импеданс x + i*y: " + ((ICircuit)CircuitFirst).CalculateImpedance(20D));
			Console.WriteLine();

			Console.ReadLine();

		}
	}
}
