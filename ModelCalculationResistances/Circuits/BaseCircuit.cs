﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
	public abstract class BaseCircuit : ICircuit
	{
		/// <summary>
		/// Компоненты(цепи, элементы) в цепи
		/// </summary>
		protected readonly List<IComponent> Components = new List<IComponent>();


		/// <summary>
		/// Получить количество элементов в цепи
		/// </summary>
		/// <returns></returns>
		public int GetCountItems()
		{
			return Components.Count;
		}


		/// <summary>
		/// Получить элемент по индексу
		/// </summary>
		/// <param name="number"> Номер элемента </param>
		public IComponent GetItem(int number)
		{
			return Components[number];
		}


		/// <summary>
		/// Метод добавления компонентов(цепи, элементов) в цепь
		/// </summary>
		/// <param name="component"> Добавляемый компонент </param>
		public void Add(IComponent component)
		{
			Components.Add(component);

			OnCircuitChenged();
		}


		/// <summary>
		/// Метод удаления компонентов(цепи, элементов) в цепь
		/// </summary>
		/// <param name="component"> Удалеяемый компонент </param>
		public void Remove(IComponent component)
		{
			Components.Remove(component);

			OnCircuitChenged();
		}


		/// <summary>
		/// Событие при изменении параллельной цепи 
		/// </summary>
		public event EventHandler CircuitChenged;


		/// <summary>
		/// Метод для запуска события CircuitChenged
		/// </summary>
		protected virtual void OnCircuitChenged()
		{
			CircuitChenged?.Invoke(this, EventArgs.Empty);
		}


		/// <summary>
		/// Расчет Импеданса
		/// </summary>
		/// <param name="frequency"> Входная частота </param>
		public abstract Complex CalculateImpedance(double frequency);
	}
}
