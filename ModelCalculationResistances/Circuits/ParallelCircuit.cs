﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using ModelCalculationResistances;

namespace ModelCalculationResistances
{
	/// <summary>
	/// Последовательная цепь
	/// </summary>
	public class ParallelCircuit : BaseCircuit
	{
		/// <summary>
		/// Расчет Импеданса
		/// </summary>
		/// <param name="frequency"> Входная частота цепи </param>
		public override Complex CalculateImpedance(double frequency)
		{
			Complex impedance = new Complex();

			for (int i = 0; i < Components.Count; ++i)
			{
				impedance += 1 / Components[i].CalculateImpedance(frequency);
			}

			impedance = 1 / impedance;

			return impedance;
		}
	}
}
