﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
	/// <summary>
	/// Последовательная цепь
	/// </summary>
	public class SerialCircuit : BaseCircuit
	{
		/// <summary>
		/// Расчет Импеданса цепи
		/// </summary>
		/// <param name="frequency"> Входная частота цепи </param>
		public override Complex CalculateImpedance(double frequency)
		{
			Complex impedance = new Complex();

			for (int i = 0; i < Components.Count; ++i)
			{
				impedance += Components[i].CalculateImpedance(frequency);
			}

			return impedance;
		}
	}
}
