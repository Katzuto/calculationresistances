﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
    /// <summary>
    /// Класс электрической цепи
    /// </summary>
    public interface ICircuit : IComponent
    {
	    /// <summary>
	    /// Расчет Импеданса электрической цепи
	    /// </summary>
	    /// <param name="frequencies"> Входные значения частот </param>
	    Complex CalculateImpedance(double frequencies);
    }
}
