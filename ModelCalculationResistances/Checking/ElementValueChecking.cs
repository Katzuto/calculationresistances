﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
    /// <summary>
    /// Класс проверки данных ModelCalculationResistances
    /// </summary>
    public class ElementValueChecking
    {
        /// <summary>
        /// Проверка значения, равно ли бесконечности или NaN
        /// </summary>
        /// <param name="value"> Проверяемое значение </param>
        public static void AssertInfAndNaN(double value)
        {
            if (double.IsNaN(value))
            {
                throw new ArgumentException("The value is NaN.");
            }

            if ((double.IsNegativeInfinity(value)) || (double.IsPositiveInfinity(value)))
            {
                throw new ArgumentException("The value is Negative or Positive Infinity.");                
            }
        }

        /// <summary>
        /// Проверка значения, меньше или равно нулю
        /// </summary>
        /// <param name="value"> Проверяемое значение </param>
        public static void AssertBelowOrEqualZero(double value)
        {
            if (value <= 0)
            {
                throw new ArgumentException("The value below or equal zero.");
            }
        }
    }
}
