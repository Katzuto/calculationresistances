﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
    /// <summary>
    /// Интерфейс элементов электрической цепи
    /// </summary>
    public interface IElement : IComponent
    {
        /// <summary>
        /// Имя элемента
        /// </summary>
        string Name { get; }


        /// <summary>
        /// Основное значение элемента
        /// (сопротивление, емкость, индукция)
        /// </summary>
        double ValueElement { get; }


		/// <summary>
		/// Расчет Импеданса
		/// </summary>
		Complex CalculateImpedance(double frequency);
    }
}
