﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
	/// <summary>
	/// Компоновщик подцепей цепей и элементов электрической цепи
	/// </summary>
	public interface IComponent
	{
		/// <summary>
		/// Расчет Импеданса
		/// </summary>
		/// <param name="frequency"> Входная частота </param>
		Complex CalculateImpedance(double frequency);


		/// <summary>
		/// Метод добавления компонентов
		/// </summary>
		/// <param name="component"> Добавляемый компонент </param>
		void Add(IComponent component);


		/// <summary>
		/// Метод удаления компонентов
		/// </summary>
		/// <param name="component"> Удаляемый компонент </param>
		void Remove(IComponent component);
	}
}
