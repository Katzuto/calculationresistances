﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
    /// <summary>
    /// Элемент Резистор
    /// </summary>
    public class Resistor : BaseElement
    {
		/// <summary>
		/// Конструктор
		/// </summary>
		public Resistor()
		{ }


		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="resistance"> Входной параметр сопротивления </param>
		public Resistor(double resistance) : base(resistance)
		{ }


		/// <summary>
		/// Имя элемента
		/// </summary>
		public override string Name => "Resistor";


		/// <summary>
		/// Расчет Импеданса
		/// </summary>
		/// <param name="frequency"> Входное значение частоты </param>
		public override Complex CalculateImpedance(double frequency)
		{
			Complex impedance = new Complex(ValueElement, 0);
			return impedance;
		}
	}
}
