﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
	public abstract class BaseElement : IElement
	{
		/// <summary>
		/// Значение элемента(емкость, индуктивность, сопротивление)
		/// </summary>
		private double _valueElement;


		/// <summary>
		/// Аксессор имени элемента
		/// </summary>
		public abstract string Name { get; }


		/// <summary>
		/// Конструктор
		/// </summary>
		protected BaseElement()
		{ }


		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="valueElement"> Значение элемента </param>
		protected BaseElement(double valueElement)
		{
			ValueElement = valueElement;
		}


		/// <summary>
		/// Событие при изменении значения элемента
		/// </summary>
		public event EventHandler ValueChanged;


		/// <summary>
		/// Метод для запуска события ValueChanged
		/// </summary>
		protected virtual void OnValueChanged()
		{
			// Эквивалентно if (ValueChanged != null)
			ValueChanged?.Invoke(this, EventArgs.Empty);
		}


		/// <summary>
		/// Аксессор Значения элемента
		/// </summary>
		public double ValueElement
		{
			get { return _valueElement; }
			set
			{
				ElementValueChecking.AssertBelowOrEqualZero(value);
				ElementValueChecking.AssertInfAndNaN(value);

				_valueElement = value;

				OnValueChanged();
			}
		}


		/// <summary>
		/// Расчет Импеданса
		/// </summary>
		/// <param name="frequencу"> Входная частота </param>
		public abstract Complex CalculateImpedance(double frequencу);


		/// <summary>
		/// Метод добавления элементов в цепь
		/// </summary>
		/// <param name="component"> Добавляемый компонент </param>
		public void Add(IComponent component)
		{
			throw new NotImplementedException();
		}


		/// <summary>
		/// Метод удаления элементов из цепи
		/// </summary>
		/// <param name="component"> Удаляемый компонент </param>
		public void Remove(IComponent component)
		{
			throw new NotImplementedException();
		}
	}
}
