﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
    /// <summary>
    /// Элемент Катушка индуктивности
    /// </summary>
    public class Inductor : BaseElement // IElement
    {
		/// <summary>
		/// Конструктор
		/// </summary>
		public Inductor()
		{ }


		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="induction"> Входной параметр индукция</param>
		public Inductor(double induction) : base(induction)
		{ }


		/// <summary>
		/// Имя элемента
		/// </summary>
		public override string Name => "Inductor";


		/// <summary>
		/// Расчет Импеданса
		/// </summary>
		/// <param name="frequency"> Входное значение частоты </param>
		public override Complex CalculateImpedance(double frequency)
		{
			Complex impedance = new Complex(0, 2 * Math.PI * frequency * ValueElement);
			return impedance;
		}
	}
}
