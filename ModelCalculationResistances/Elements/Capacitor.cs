﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ModelCalculationResistances
{
    /// <summary>
    /// Элемент Конденсатор
    /// </summary>
    public class Capacitor : BaseElement // IElement
    {
		/// <summary>
		/// Конструтор
		/// </summary>
		public Capacitor()
		{ }

		/// <summary>
		/// Конструтор
		/// </summary>
		/// <param name="capacity"> Входной параметр емкость </param>
		public Capacitor(double capacity) : base(capacity)
		{ }


		/// <summary>
		/// Имя элемента
		/// </summary>
		public override string Name => "Capacitor";


		/// <summary>
		/// Рассчет Импеданса
		/// </summary>
		/// <param name="frequency"> Входное значение частоты </param>
		public override Complex CalculateImpedance(double frequency)
		{
			Complex impedance = new Complex(0, (-1 / (2 * Math.PI * frequency * ValueElement)));
			return impedance;
		}
	}
}
