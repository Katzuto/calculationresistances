﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelCalculationResistances;
using NUnit.Framework;


namespace UnitTests
{
    /// <summary>
    /// Набор тестов для класса Resistor
    /// </summary>
    [TestFixture]
    class ResistorTest
    {
		/// <summary>
		/// Тестирование конструктора Resistance
		/// </summary>
		/// <param name="resistance"> Значение свойства Value </param>
		[Test]
		[TestCase(4, TestName = "Тестирование конструктора при присваивании 4.")]
		[TestCase(0.001, TestName = "Тестирование конструктора при присваивании 0.001.")]
		[TestCase(1, TestName = "Тестирование конструктора при присваивании 1.")]
		[TestCase(double.MaxValue, TestName = "Тестировании конструктора при" +
											  " присваивании MaxValue.")]
		[TestCase(double.MaxValue - 1, TestName = "Тестирование конструктора при" +
												  " присваивании MaxValue - 1.")]
		public void ResistanceСonstructorTest(double resistance)
		{
			BaseElement resistor = new Resistor(resistance);
		}


		/// <summary>
		/// Тестирование конструктора Resistance
		/// </summary>
		/// <param name="resistance"> Значение свойства Value </param>
		[Test]
		[TestCase(double.NegativeInfinity, TestName = "Тестирование Value " +
			"при присвоении NegativeInfinity.")]
		[TestCase(double.NaN, TestName = "Тестирование Value при " +
			"присвоении NaN.")]
		[TestCase(double.PositiveInfinity, TestName = "Тестирование Value " +
			"при присваивании PositiveInfinity.")]
		[TestCase(double.MinValue, TestName = "Тестирование Value при " +
			"присваивании MinValue.")]
		[TestCase(-1, TestName = "Тестирование Value при " +
			"присваивании -1.")]
		[TestCase(0, TestName = "Тестирование Value при присваивании " +
			"0.")]
		public void ResistanceСonstructorTestThrowExceprion(double resistance)
		{
			Assert.Throws<ArgumentException>(() => new Resistor(resistance));
		}


		/// <summary>
		/// Тестирование свойства Value
		/// </summary>
		/// <param name="resistance"> Значение свойства Resistance </param>
		[Test]
        [TestCase(4, TestName = "Тестирование Value при присваивании 4.")]
        [TestCase(0.001, TestName = "Тестирование Value при присваивании 0.001.")]
        [TestCase(1, TestName = "Тестирование Value при присваивании 1.")]
        [TestCase(double.MaxValue, TestName = "Тестировании Value при" +
                                              " присваивании MaxValue.")]
        [TestCase(double.MaxValue - 1, TestName = "Тестирование Value при" +
                                                  " присваивании MaxValue - 1.")]
        public void ResistanceValueTest(double resistance)
        {
			BaseElement resistor = new Resistor();
            resistor.ValueElement = resistance;
        }


		/// <summary>
		/// Тестирование свойства Value на выброс Exception'а
		/// </summary>
		/// <param name="resistance"> Значение свойства Value </param>
		[Test]
        [TestCase(double.NegativeInfinity, TestName = "Тестирование Value " +
            "при присвоении NegativeInfinity.")]
        [TestCase(double.NaN, TestName = "Тестирование Value при " + 
            "присвоении NaN.")]
        [TestCase(double.PositiveInfinity, TestName = "Тестирование Value " +
            "при присваивании PositiveInfinity.")]
        [TestCase(double.MinValue, TestName = "Тестирование Value при " + 
            "присваивании MinValue.")]
        [TestCase(-1, TestName = "Тестирование Value при " + 
            "присваивании -1.")]
        [TestCase(0, TestName = "Тестирование Value при присваивании " +
            "0.")]
        public void ResistanceValueTestThrowExceprion(double resistance)
        {
            Resistor resistor = new Resistor();

            Assert.Throws<ArgumentException>(() => resistor.ValueElement = resistance);
        }
        
    }
}