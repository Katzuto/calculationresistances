﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelCalculationResistances;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace UnitTests
{
    /// <summary>
    /// Тесты для класса Capacitor
    /// </summary>
    [TestFixture]
    class CapacitorTest
    {
		/// <summary>
		/// Тестирование конструктора Induction
		/// </summary>
		/// <param name="capacity"> Значение свойства Value </param>
		[Test]
		[TestCase(4, TestName = "Тестирование конструктора при присваивании 4.")]
		[TestCase(0.001, TestName = "Тестирование конструктора при присваивании 0.001.")]
		[TestCase(1, TestName = "Тестирование конструктора при присваивании 1.")]
		[TestCase(double.MaxValue, TestName = "Тестировании конструктора при" +
											  " присваивании MaxValue.")]
		[TestCase(double.MaxValue - 1, TestName = "Тестирование конструктора при" +
												  " присваивании MaxValue - 1.")]
		public void InductionСonstructorTest(double capacity)
		{
			BaseElement resistor = new Capacitor(capacity);
		}


		/// <summary>
		/// Тестирование конструктора Capacitor
		/// </summary>
		/// <param name="capacity"> Значение свойства Value </param>
		[Test]
		[TestCase(double.NegativeInfinity, TestName = "Тестирование конструктора " +
			"при присвоении NegativeInfinity.")]
		[TestCase(double.NaN, TestName = "Тестирование конструктора при " +
			"присвоении NaN.")]
		[TestCase(double.PositiveInfinity, TestName = "Тестирование конструктора " +
			"при присваивании PositiveInfinity.")]
		[TestCase(double.MinValue, TestName = "Тестирование конструктора при " +
			"присваивании MinValue.")]
		[TestCase(-1, TestName = "Тестирование конструктора при " +
			"присваивании -1.")]
		[TestCase(0, TestName = "Тестирование конструктора при присваивании " +
			"0.")]
		public void InductionСonstructorTestThrowExceprion(double capacity)
		{
			Assert.Throws<ArgumentException>(() => new Capacitor(capacity));
		}


		/// <summary>
		/// Тестирование свойства Value
		/// </summary>
		/// <param name="capacity"> Значение свойства Value </param>
		[Test]
        [TestCase(4, TestName = "Тестирование Value при присваивании 4.")]
        [TestCase(0.001, TestName = "Тестирование Value при присваивании 0.001.")]
        [TestCase(1, TestName = "Тестирование Value при присваивании 1.")]
        [TestCase(double.MaxValue, TestName = "Тестировании Value при" +
                                              " присваивании MaxValue.")]
        [TestCase(double.MaxValue - 1, TestName = "Тестирование Value при" +
                                                  " присваивании MaxValue - 1.")]
        public void CapacityValueTest(double capacity)
        {
            BaseElement capacitor = new Capacitor();
	        capacitor.ValueElement = capacity;
        }


		/// <summary>
		/// Тестирование свойства Value на выброс Exception'а
		/// </summary>
		/// <param name="capacity"> Значение свойства Value </param>
		[Test]
        [TestCase(double.NegativeInfinity, TestName = "Тестирование Value " +
            "при присвоении NegativeInfinity.")]
        [TestCase(double.NaN, TestName = "Тестирование Value при " +
            "присвоении NaN.")]
        [TestCase(double.PositiveInfinity, TestName = "Тестирование Value " +
            "при присваивании PositiveInfinity.")]
        [TestCase(double.MinValue, TestName = "Тестирование Value при " +
            "присваивании MinValue.")]
        [TestCase(-1, TestName = "Тестирование Value при " +
            "присваивании -1.")]
        [TestCase(0, TestName = "Тестирование Value при присваивании " +
            "0.")]
        public void CapacityValueTestThrowExceprion(double capacity)
        {
			BaseElement capacitor = new Capacitor();

            Assert.Throws<ArgumentException>(() => capacitor.ValueElement = capacity);
        }
    }
}
