﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelCalculationResistances;
using NUnit.Framework;

namespace UnitTests
{
    /// <summary>
    /// Тесты для тестирования класса Inductor
    /// </summary>
    [TestFixture]
    class InductorTest
    {
		/// <summary>
		/// Тестирование конструктора Induction
		/// </summary>
		/// <param name="induction"> Значение свойства Value </param>
		[Test]
		[TestCase(4, TestName = "Тестирование конструктора при присваивании 4.")]
		[TestCase(0.001, TestName = "Тестирование конструктора при присваивании 0.001.")]
		[TestCase(1, TestName = "Тестирование конструктора при присваивании 1.")]
		[TestCase(double.MaxValue, TestName = "Тестировании конструктора при" +
											  " присваивании MaxValue.")]
		[TestCase(double.MaxValue - 1, TestName = "Тестирование конструктора при" +
												  " присваивании MaxValue - 1.")]
		public void InductionСonstructorTest(double induction)
		{
			BaseElement resistor = new Inductor(induction);
		}


		/// <summary>
		/// Тестирование конструктора Induction
		/// </summary>
		/// <param name="induction"> Значение свойства Value </param>
		[Test]
		[TestCase(double.NegativeInfinity, TestName = "Тестирование Value " +
			"при присвоении NegativeInfinity.")]
		[TestCase(double.NaN, TestName = "Тестирование Value при " +
			"присвоении NaN.")]
		[TestCase(double.PositiveInfinity, TestName = "Тестирование Value " +
			"при присваивании PositiveInfinity.")]
		[TestCase(double.MinValue, TestName = "Тестирование Value при " +
			"присваивании MinValue.")]
		[TestCase(-1, TestName = "Тестирование Value при " +
			"присваивании -1.")]
		[TestCase(0, TestName = "Тестирование Value при присваивании " +
			"0.")]
		public void InductionСonstructorTestThrowExceprion(double induction)
		{
			Assert.Throws<ArgumentException>(() => new Inductor(induction));
		}


		/// <summary>
		/// Тестирование свойства Value
		/// </summary>
		/// <param name="induction"> Значение свойства Value </param>
		[Test]
        [TestCase(4, TestName = "Тестирование Value при присваивании 4.")]
        [TestCase(0.001, TestName = "Тестирование Value при присваивании 0.001.")]
        [TestCase(1, TestName = "Тестирование Value при присваивании 1.")]
        [TestCase(double.MaxValue, TestName = "Тестировании Value при" +
                                              " присваивании MaxValue.")]
        [TestCase(double.MaxValue - 1, TestName = "Тестирование Value при" +
                                                  " присваивании MaxValue - 1.")]
        public void InductionValueTest(double induction)
        {
            BaseElement inductor = new Inductor();
            inductor.ValueElement = induction;
        }


		/// <summary>
		/// Тестирование свойства Value на выброс Exception'а
		/// </summary>
		/// <param name="induction"> Значение свойства Value </param>
		[Test]
        [TestCase(double.NegativeInfinity, TestName = "Тестирование Value " +
            "при присвоении NegativeInfinity.")]
        [TestCase(double.NaN, TestName = "Тестирование Value при " +
            "присвоении NaN.")]
        [TestCase(double.PositiveInfinity, TestName = "Тестирование Value " +
            "при присваивании PositiveInfinity.")]
        [TestCase(double.MinValue, TestName = "Тестирование Value при " +
            "присваивании MinValue.")]
        [TestCase(-1, TestName = "Тестирование Value при " +
            "присваивании -1.")]
        [TestCase(0, TestName = "Тестирование Value при присваивании " +
            "0.")]
        public void InductionValueTestThrowExceprion(double induction)
        {
			BaseElement inductor = new Inductor();

            Assert.Throws<ArgumentException>(() => inductor.ValueElement = induction);
        }
    }
}
