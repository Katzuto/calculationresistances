﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelCalculationResistances;
using NUnit.Framework;

namespace UnitTests
{
	/// <summary>
	/// Тесты для ElementValueChecking
	/// </summary>
	public class ElementValueCheckingTest
	{
		/// <summary>
		/// Тестирование метода AssertInfAndNaN
		/// </summary>
		/// <param name="value"> Теструемое значение </param>
		[Test]
		[TestCase(double.MinValue, TestName = "Тестирование AssertInfAndNaN при " +
			"присваивании MinValue.")]
		[TestCase(-1, TestName = "Тестирование AssertInfAndNaN при " +
			"присваивании -1.")]
		[TestCase(0, TestName = "Тестирование AssertInfAndNaN при присваивании " +
			"0.")]
		public void AssertInfAndNaNTest(double value)
		{
			ElementValueChecking.AssertInfAndNaN(value);
		}


		/// <summary>
		/// Тестирование метода AssertInfAndNaN
		/// </summary>
		/// <param name="value"> Теструемое значение </param>
		[Test]
		[TestCase(double.NegativeInfinity, TestName = "Тестирование AssertInfAndNaN " +
			"при присвоении NegativeInfinity.")]
		[TestCase(double.NaN, TestName = "Тестирование AssertInfAndNaN при " +
			"присвоении NaN.")]
		[TestCase(double.PositiveInfinity, TestName = "Тестирование AssertInfAndNaN " +
			"при присваивании PositiveInfinity.")]
		public void AssertInfAndNaNTestThrowExceprion(double value)
		{
			Assert.Throws<ArgumentException>(() => ElementValueChecking.AssertInfAndNaN(value));
		}


		/// <summary>
		/// Тестирование метода AssertBelowOrEqualZero
		/// </summary>
		/// <param name="value"> Теструемое значение </param>
		[Test]
		[TestCase(1, TestName = "Тестирование AssertBelowOrEqualZero при " +
			"присваивании 1.")]
		[TestCase(100, TestName = "Тестирование AssertBelowOrEqualZero при присваивании " +
			"100.")]
		[TestCase(double.MaxValue, TestName = "Тестирование AssertBelowOrEqualZero при " +
			"присваивании MaxValue.")]
		public void AssertBelowOrEqualZeroTest(double value)
		{
			ElementValueChecking.AssertBelowOrEqualZero(value);
		}


		/// <summary>
		/// Тестирование метода AssertBelowOrEqualZero
		/// </summary>
		/// <param name="value"> Теструемое значение </param>
		[Test]
		[TestCase(double.MinValue, TestName = "Тестирование AssertBelowOrEqualZero при " +
			"присваивании MinValue.")]
		[TestCase(-1, TestName = "Тестирование AssertBelowOrEqualZero при " +
			"присваивании -1.")]
		[TestCase(0, TestName = "Тестирование AssertBelowOrEqualZero при присваивании " +
			"0.")]
		public void AssertBelowOrEqualZeroTestThrowExceprion(double value)
		{
			Assert.Throws<ArgumentException>(() => ElementValueChecking.AssertBelowOrEqualZero(value));
		}
	}
}
